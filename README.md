# Composer
**A docker container based on `php:fpm` which comes with `composer` installed.**

## How to use this image
### Adding files
Simply copy all your required files inside `WORKDIR` and start the container.

### Cloning a GIT repository
Define the `url` of the GIT repository you want to use inside `GIT_REPO`. The container will clone your repository into `WORKDIR` on the each start, especialy the initial start, as long `WORDDIR` is empty.

### Updating a GIT repository
If `GIT_REPO` is defined and the initial cloning (*see `Cloning a GIT repository`*) was performed, an automatic pull of that repository will be perfomed on each start of the container, as long `GIT_UPDATE` is set to `true` and the `.git` folder exists.

### Composer install
By default the container will run `composer install` on each start of the container as long these conditions are met:
* `WORKDIR` must contain `composer.json`
* `WORKDIR` must not contain a `vendor` folder

To disable this feature you have to set `INSTALL_COMPOSER` to `false`.

*Note: `Composer install` will be executed after `Cloning a GIT repository` for a better user experience.*

### Standalone
Just start the container and enjoy `php` and `composer`.

## Image variants
All container share the same version of `composer`, installed packages, and `docker php extensions` (besides any php version specific extension).
The tag of each of these containers matched the container of php-fpm. For example: `boris71s/composer:8.0` uses `php:8.0-fpm` as the base image. This way it is easy to switch between versions.

## Environment Variables
| ENV | Value(s) | Definition |
| --- | --- | --- |
| `COMPOSER_INSTALL` | `false`,<br>`true`<br>default: `true` | If set, the container will run `composer install` inside `WORKDIR` on startup.<br><br>*Behaviour:<br>- `WORKDIR` must contain `composer.json`<br>- `WORKDIR` must not contain a `vendor` folder*
| `GIT_REPO` | `url`<br>default: `null` | If set with a valid url, the container runs the `git clone {url} .` command inside `WORKDIR`.<br><br>*Behaviour:<br>- `WORKDIR` has to be empty* |
| `GIT_UPDATE` | `false`,<br>`true`<br>default: `false` | If set to `true`, then the container will perform a `git pull` on each start.<br><br>*Behaviour:<br>- `GIT_REPO` must be a vilid url<br>- `WORKDIR` must contain a `.git` folder* |
| `WORKDIR` | `/code` | The directory which contains all of your code. |

## Internal Environment Variables
| ENV | Value(s) | Definition |
| --- | --- | --- |
| `VENDOR_ROOT` | `/vendor` | Location of all files and folders shipped with the container |


## Installed packages
* apt-utils
* curl
* git
* unzip

## Installed docker php extension
* bz2
* @composer
* exif
* gd
* intl
* mbstring
* pdo
* pdo_mysql
* zip

---
Check out the [docker repository](https://hub.docker.com/r/boris71s/composer) for a list of versions.