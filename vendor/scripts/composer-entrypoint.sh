#!/bin/bash

source "${VENDOR_ROOT}"/scripts/composer-shared.sh || exit

if is_git_cloneable; then
  git clone "${GIT_REPO}" .
elif is_git_pullable; then
  git pull
fi

if is_set_install_composer && is_composer_installable; then
  composer install
fi

exec "$@"
