#!/bin/bash

cd "${WORKDIR}" || exit

# @ env checks
is_set_git_repo() {
  [ ! "${GIT_REPO}" = null ]
}

is_set_install_composer() {
  [ ! "${INSTALL_COMPOSER}" = false ]
}

# @ helpers
composer_json_exists() {
  [ -f "composer.json" ]
}

is_root_empty() {
  [ -n "$(find "${WORKDIR}" -prune -empty)" ]
}

remote_repo_exists() {
  curl --output /dev/null --silent --head --fail "${GIT_REPO}"
}

# @ *able checks
is_composer_installable() {
  composer_json_exists && [ ! -d "vendor" ]
}

is_git_cloneable() {
  is_root_empty && is_set_git_repo && remote_repo_exists
}

is_git_pullable() {
  [ -d ".git" ] && is_set_git_update && remote_repo_exists
}
