#!/bin/bash

# list of all tags to build,
tags=(
  8.0
  8.1
  8.2
)
repository=boris71s/composer

# build all defined tags
for tag in ${tags[*]}; do
  printf "building %s\n" "${repository}":"${tag}"
  docker build -t "${repository}":"${tag}" --build-arg BASE_TAG="${tag}"-fpm .
  printf "\n%s build complete\n\n\n" "${repository}":"${tag}"
done
# tag latest built as latest
docker tag "${repository}":"${tags[-1]}" "${repository}":latest

# push all tags
for tag in ${tags[*]}; do
  printf "pushing %s\n" "${repository}":"${tag}"
  docker push "${repository}":"${tag}"
  printf "\n%s push complete\n\n\n" "${repository}":"${tag}"
done
docker push "${repository}":latest