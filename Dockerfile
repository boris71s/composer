ARG BASE_TAG=fpm
FROM php:${BASE_TAG}

# define container variables
ENV COMSPOSER_INSTALL=true \
    GIT_REPO=null \
    GIT_UPDATE=false \
    VENDOR_ROOT=/vendor \
    WORKDIR=/code

# install essential tools
ENV ESSENTIAL_TOOLS \
        apt-utils \
        curl \
        git \
        locales \
        unzip

RUN apt-get update && \
    apt-get install -y $ESSENTIAL_TOOLS

# automated extensions installer
ENV PHP_EXTENTIONS \
        bz2 \
        @composer \
        exif \
        gd \
        intl \
        pdo_mysql \
        zip

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    sync && \
    install-php-extensions $PHP_EXTENTIONS

# add vendor files
ADD ./vendor ${VENDOR_ROOT}

# make script executable
RUN chmod +x ${VENDOR_ROOT}/scripts/*.sh

# set working directory
WORKDIR ${WORKDIR}

# define entrypoint
ENTRYPOINT ["/vendor/scripts/composer-entrypoint.sh"]

# match php:fpm cmd
CMD ["php-fpm"]